/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parimparserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TGPSI-2ano
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static boolean continuar = true;
    static ServerSocket serverSocket;
    static List<Socket> clients = new ArrayList<>();
    static Thread connectionThread;
    static int quantidade = 0;

    public static void main(String[] args) {
        startServer();

    }

    private static synchronized void startServer() {
        Thread RunThread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    serverSocket = new ServerSocket(9009);
                    while (continuar) {

                        if (!continuar) {
                            break;
                        }
                        Socket socket = serverSocket.accept();
                        clients.add(socket);
                        connectionThread = new Thread(new Runnable() {

                            @Override
                            public void run() {
                                try {
                                    System.out.println("Nova ligação (" + socket.toString() + ")");
                                    DataInputStream input = new DataInputStream(socket.getInputStream());
                                    DataOutputStream output = new DataOutputStream((socket.getOutputStream()));
                                    output.writeUTF("WELCOME");
                                    int numero;

                                    do {
                                        quantidade++;
                                        numero = input.readInt();
                                        System.out.println("Numero recebido: " + numero);

                                        if (numero % 2 == 0) {
                                            output.writeUTF("PAR -> " + numero);
                                            //System.out.println("PAR");
                                        } else {
                                            output.writeUTF("IMPAR -> " + numero);
                                            //System.out.println("IMPAR");
                                        }

                                    } while (numero != 0);
                                    terminateServer();
                                } catch (IOException ex) {
                                    //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        });
                        connectionThread.start();
                    }
                } catch (IOException ex) {
                    //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
        RunThread.start();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Escreva FIM para encerrar o servidor");
        if (scanner.nextLine().equalsIgnoreCase("fim")) {
            terminateServer();
        }
    }

    private static void terminateServer() {
        System.out.println("Terminando servidor");
        try {
            continuar = false;
            serverSocket.close();
        } catch (IOException ex) {
            //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (Socket s : clients) {
            try {
                s.close();
            } catch (IOException ex) {
                //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            Socket socket = new Socket("127.0.0.1", 9009);
            socket.close();
        } catch (IOException ex) {
            //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        connectionThread.interrupt();
        System.out.println("Total de numeros recebidos: "+quantidade);
        System.exit(0);

    }

}
